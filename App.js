/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Fragment} from 'react';
import ReactNativeWebHelloWorld from './app/native/containers/App';

const App = () => {
  return (
    <ReactNativeWebHelloWorld color="blue"/>
  );
};

export default App;
