const path = require('path');

module.exports = {
  root: path.resolve(__dirname, '../'),
  outputPath: path.resolve(__dirname, '../', 'build'),
  entryPath: path.resolve(__dirname, '../../', 'app/web/index.js'),
  templatePath: path.resolve(__dirname, '../', 'public/index.html'),
  imagesFolder: 'assets',
  fontsFolder: 'fonts',
  cssFolder: 'css',
  jsFolder: 'js',
};
