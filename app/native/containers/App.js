import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { appStyle } from '../styles/styles';
// dumb components
import Header     from '../components/Header';
import HelloWorld from '../components/HelloWorld';

/** The app entry point */
export default class ReactNativeWebHelloWorld extends Component {
  render() {
    // injected by connect call
    const { color } = this.props;

    return (
      <View style={appStyle.reactNativeWeb}>
        <Header />
        <HelloWorld
          color={color}
        />
      </View>
    );
  }
}

ReactNativeWebHelloWorld.propTypes = {
  color: PropTypes.string.isRequired
};