import React, { Component } from 'react';
import PropTypes from 'prop-types';
// dumb components
import Header from '../components/Header';
import HelloWorld from '../components/HelloWorld';

/** The app entry point */
export default class ReactNativeWebHelloWorld extends Component {
  render() {
    // injected by connect call
    const { color } = this.props;

    return (
      <div className="react-native-web">
        <Header />
        <HelloWorld
          color={color}
        />
      </div>
    );
  }
}

ReactNativeWebHelloWorld.propTypes = {
  color: PropTypes.string.isRequired
};