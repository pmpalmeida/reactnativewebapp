/**
 * @format
 */

import React from 'react';
import { render } from 'react-dom';
import ReactNativeWebHelloWorld from './containers/App';

// load our css
require('./styles/style.scss');

const rootElement = document.getElementById('app');

render(<ReactNativeWebHelloWorld color="red" />, rootElement);