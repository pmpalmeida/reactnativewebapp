import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class HelloWorld extends Component {
  render() {
    const { color } = this.props;
    return (
      <div className="hello-world" style={{color: color}}>Hello World</div>
    );
  }
}

HelloWorld.propTypes = {
  color: PropTypes.string.isRequired
}